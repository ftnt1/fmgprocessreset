#!/usr/bin/env python3

"""fazresetoftp.py: It logs in to FortiManager or FortiAnalyzer via SSH, gets the process list, extracts process PID and restarts it with signal 11. It requires 'paramiko' to function"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.2"
__status__ = "Production"
__requires__ = ['paramiko']

import paramiko
import time

# FMG / FAZ configuration
server_IP = "10.109.19.70"
username = "admin"
password = ""
port = 22
service = "oftpd"

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(server_IP,port,username, password)
stdin, stdout, stderr = ssh.exec_command("diag sys process list")

output = stdout.read().splitlines()
pidbefore = []
pidafter = []
for line in output:
    if service in line.decode("utf-8"):
        data1 = line.decode("utf-8").split()
        print("Service " + service + " found with PID " + data1[0])
        pidbefore.append(data1[0])
        print("resetting PID " + data1[0])
        command = "diag sys process kill -11 " + data1[0]
        stdin, stdout, stderr = ssh.exec_command(command)
        stdout.read()

print("\nWaiting 5 seconds")
time.sleep(5)

print("\nDone resetting " + service + "\nIt should have new PID now\n\n")		
stdin, stdout, stderr = ssh.exec_command("diag sys process list")
output = stdout.read().splitlines()
for line in output:
    if service in line.decode("utf-8"):
        data2 = line.decode("utf-8").split()
        print("Service " + service + " found with PID " + data2[0])
        pidafter.append(data2[0])
if pidbefore==pidafter:
    print("\nFAIL\n")
else:
    print("\nSUCCESS\n")